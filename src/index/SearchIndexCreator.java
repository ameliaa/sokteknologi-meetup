package index;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.QuestionAnswerObject;
import model.SearchIndex;

public class SearchIndexCreator {

	private static final String QUESTIONS_FILE_NAME = "resources/questions.txt";
	private static final String ANSWERS_FILE_NAME = "resources/answers.txt";

	public SearchIndex generateIndex() {
		return read();
	}

	public SearchIndex read() {
		SearchIndex index = new SearchIndex();
		
		Scanner questionScanner = getScannerForFile(QUESTIONS_FILE_NAME);
		Scanner answersScanner = getScannerForFile(ANSWERS_FILE_NAME);

		int questionNumber = 0;

		while (questionScanner.hasNextLine()) {
			
			String questionLine = questionScanner.nextLine();
			String question = removeLeadingQuestionNumber(questionLine, questionNumber);

			List<String> answers = getAnswersForQuestion(answersScanner, questionNumber);

			QuestionAnswerObject questionAndAnswerObject = new QuestionAnswerObject(question, answers);

			//TODO: Write your own algorithm for indexing answer content and use that instead
			SearchIndexCreatorAlgorithms.fillIndexBeginningWord5Ngram(questionAndAnswerObject, index);
			
			questionNumber++;
		}
		
		return index;
	}

	private Scanner getScannerForFile(String fileName) {
		InputStream inputStream = this.getClass().getClassLoader()
				.getResourceAsStream(fileName);
		
		return new Scanner(inputStream);
	}

	private List<String> getAnswersForQuestion(Scanner answersScanner, int currentIndex) {
		List<String> answers = new ArrayList<String>();		
		while(answersScanner.hasNext(""+currentIndex)){
			String answerLine = answersScanner.nextLine();
			String answer = removeLeadingQuestionNumber(answerLine, currentIndex);
			answers.add(answer);
		} 
		return answers;
	}

	private String removeLeadingQuestionNumber(String textLine, int indexNumber) {
		return textLine.replaceFirst("" + indexNumber + " ", "");
	}
	
	
}
