package index;

import java.util.List;

import model.QuestionAnswerObject;
import model.SearchIndex;

public class SearchIndexCreatorAlgorithms {

	/**
	 * Example index creator algorithm
	 * 
	 * This method adds a 5-gram based on the text found in the answers.txt 
	 * file to the index. The third word in the ngram is the key.
	 * 
	 * Example sentence: For Christmas I would like world peace
	 * 
	 * I -> For Christmas I would like 
	 * would -> Christmas I would like world
	 * like -> I would like world peace
	 * 
	 * @param answers
	 * @param index
	 */

	public static void fillIndexMiddleWord5Ngram(List<String> answers,
			SearchIndex index) {
		for (String answer : answers) {
			String[] answerArray = answer.split(" ");
			for (int i = 2; i < answerArray.length - 2; i++) {
				String key = answerArray[i];
				String ngram = answerArray[i - 2] + " " + answerArray[i - 1]
						+ " " + answerArray[i] + " " + answerArray[i + 1] + " "
						+ answerArray[i + 2];
				index.index(key, ngram);
			}
		}
	}

	/**
	 * Example index creator algorithm
	 * 
	 * This method adds a 5-gram based on the text found in the answers.txt 
	 * The first word in the ngram is the word after the key
	 *  
	 * Example sentence: For Christmas I would like world peace forever
	 * 
	 * For -> Christmas I would like world
	 * Christmas -> I would like world peace
	 * I -> would like world peace forever
	 *
	 */
	public static void fillIndexBeginningWord5Ngram(
			QuestionAnswerObject questionAnswerObject, SearchIndex index) {
		List<String> answers = questionAnswerObject.getAnswers();
		for (String answer : answers) {
			String[] answerArray = answer.split(" ");
			for (int i = 0; i < answerArray.length - 6; i++) {
				String key = answerArray[i];
				String ngram = answerArray[i + 1] + " " + answerArray[i + 2]
						+ " " + answerArray[i + 3] + " " + answerArray[i + 4]
						+ " " + answerArray[i + 5];
				index.index(key, ngram);
			}
		}
	}
	
	/**
	 *  TODO: Create your own algorithm
	 */
	public static void myIndexingAlgorithm(
			QuestionAnswerObject message, SearchIndex index) {
		
	}
	
}
