package main;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import bot.examples.SimpleAnswerBot;
import model.SearchIndex;
import bot.AnswerBot;
import bot.BotAnswerPrinter;
import bot.examples.NgramAnswerBot;
import index.SearchIndexCreator;

public class Main {
	private SearchIndex index;

	public static void main(String[] args) throws IOException, InterruptedException {
		if(args.length == 0){
			BotAnswerPrinter.noBotChoosen();
			System.exit(0);
		}
		String botName = args[0];
		new Main().startBot(botName);
	}

	private void startBot(String botName) throws IOException, InterruptedException {

		//TODO Advanced Create your own index
		SearchIndexCreator searchIndexCreator = new SearchIndexCreator();
		index = searchIndexCreator.generateIndex();

		//Chose which bot to use
		//TODO Add your own bot here if you implemented a new one
		List<AnswerBot> availableBots = Arrays.asList(
				new SimpleAnswerBot(index),
				new NgramAnswerBot(index)
		);
		AnswerBot bot = null;
		for (AnswerBot availableBot : availableBots) {
			if(availableBot.getBotName().equals(botName)){
				bot = availableBot;
				break;
			}
		}
		if(bot == null){
			BotAnswerPrinter.couldNotFoundBot(botName);
			System.exit(0);
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		while (true) {
			System.out.println("Skriv en fråga så svarar jag :) ");
			
			// Reads the user question
			String userQuestion = br.readLine();

			// Returns an answer based on the user question
			Optional<String> maybeAnswer = bot.answer(userQuestion);
			
			// Simulates thinking time
			Thread.sleep(1000);
			if(maybeAnswer.isPresent()){
				BotAnswerPrinter.printAnswerQuestion(maybeAnswer.get());
			}else{
				BotAnswerPrinter.printCouldNotFindInformation();
			}

		}

	}
}
