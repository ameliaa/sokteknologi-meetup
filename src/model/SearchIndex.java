package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the search index.
 * 
 * We store questions words as keys and (parts of) answers as values
 * 
 * Example: 
 * 
 * If we have the question:
 * 
 * What is my name?
 * 
 * And the parts of answers are:
 * 
 * Your name is Bertil
 * Your name is Elsa
 * 
 * The index will look something like this:
 * 
 * What -> {Your name is Bertil, Your name is Elsa}
 * is -> {Your name is Bertil, Your name is Elsa}
 * my -> {Your name is Bertil, Your name is Elsa}
 * name -> {Your name is Bertil, Your name is Elsa}
 * 
 * This means that when you "search" for the word [name]
 * it will return a list of both the answers
 * "Your name is Bertil" and "Your name is Elsa".
 * 
 * In our examples we have indexed the answers as ngrams.
 */
public class SearchIndex {
    private Map<String, List<String>> index;

    public SearchIndex(String filePath) {
        //serialize
    }

    public SearchIndex() {
        index = new HashMap<>();
    }

    public void index(String key, String ngram) {
    	key = key.toLowerCase();
        List<String> ngrams;
        if (index.containsKey(key)) {
            ngrams = index.get(key);
        } else {
            ngrams = new ArrayList<>();
        }
        ngrams.add(ngram);

        index.put(key, ngrams);
    }

    public List<String> search(String key) {
    	key = key.toLowerCase();
        if (index.containsKey(key)) {
            return index.get(key);
        } else {
            return new ArrayList<>();
        }
    }

    public int size(String key) {
    	key = key.toLowerCase();
        if (index.containsKey(key)) {
            return index.get(key).size();
        } else {
            return 0;
        }
    }
}
