package bot;

import model.SearchIndex;

public abstract class AbstractSearchingAnswerBot implements AnswerBot {
    protected SearchIndex index;

    public AbstractSearchingAnswerBot(SearchIndex index) {
        this.index = index;
    }

}
