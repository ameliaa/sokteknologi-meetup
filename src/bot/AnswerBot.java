package bot;

import java.util.Optional;

public interface AnswerBot {
    Optional<String> answer(String question);

    String getBotName();
}
