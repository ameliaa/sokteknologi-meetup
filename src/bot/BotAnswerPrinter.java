package bot;

public class BotAnswerPrinter {

	public static void printAnswerQuestion(String answer) {
		StringBuilder answerPrinter = new StringBuilder();
		answerPrinter.append("\n");
		answerPrinter.append(answer);
		answerPrinter.append("\n");
		answerPrinter.append("\n");
		slowlyPrint(answerPrinter.toString().toCharArray());
	}

	public static void printCouldNotFindInformation() {
		StringBuilder couldNotFindInforationPrinter = new StringBuilder();
		couldNotFindInforationPrinter.append("\n");
		couldNotFindInforationPrinter.append("Å nej det där där vet jag inget om :(" +
                " men pröva gärna med något annat.");
		couldNotFindInforationPrinter.append("\n");
		slowlyPrint(couldNotFindInforationPrinter.toString().toCharArray());
	}

	public static void printBotWillHelpMessage(String searchQuestion) {
		StringBuilder helpMessagePrinter = new StringBuilder();
		helpMessagePrinter.append("\n");
		helpMessagePrinter.append("Aha du vill veta mer om: "
				+ searchQuestion + " Låt oss se vad vi kan hitta.");
		helpMessagePrinter.append("\n");
		slowlyPrint(helpMessagePrinter.toString().toCharArray());
	}

	public static void couldNotFoundBot(String botname) {
		StringBuilder couldNotFoundBotPrinter = new StringBuilder();
		couldNotFoundBotPrinter.append("\n");
		couldNotFoundBotPrinter.append("Tyvärr hittade vi ingen bot med namnet: "
				+ botname + " Lägg till din bot bland available bots i Main klassen och kör igen!");
		couldNotFoundBotPrinter.append("\n");
		slowlyPrint(couldNotFoundBotPrinter.toString().toCharArray());

	}

    public static void noBotChoosen(){
        StringBuilder noBotChosenPrinter = new StringBuilder();
        noBotChosenPrinter.append("\n");
        noBotChosenPrinter.append("Du måste skicka med ett namn på en bot när du startar programmet, försök igen :)");
        noBotChosenPrinter.append("\n");
        slowlyPrint(noBotChosenPrinter.toString().toCharArray());
    }

	private static void slowlyPrint(char[] charArray) {
		try {
			for (int i = 0; i < charArray.length; i++) {
				
				System.out.print(charArray[i]);
				if(charArray[i] == '.') {
					System.out.println("\n");
				}
				Thread.sleep(50);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
