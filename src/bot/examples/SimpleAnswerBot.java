package bot.examples;

import bot.AbstractSearchingAnswerBot;
import bot.BotAnswerPrinter;
import model.SearchIndex;

import java.util.List;
import java.util.Optional;

public class SimpleAnswerBot extends AbstractSearchingAnswerBot {

    public SimpleAnswerBot(SearchIndex index) {
        super(index);
    }

    /**
     * This bot will take a random word from your question and build an answer
     * using the random word as the key.
     */
    @Override
    public Optional<String> answer(String question) {

        //Extracts a search word from the question
        ExampleQuestionParser questionParser = new ExampleQuestionParser();

        //TODO: Modify the question parser or create your own question parser, for extracting one or many search words
        String searchWord = questionParser.getWordToSearchOn(question);

        // Prints information it will search on
        BotAnswerPrinter.printBotWillHelpMessage(searchWord);

        // Checks if the chosen word can be found in the index
        // If the index doesn't contain the word an empty Optional is returned
        if (index.size(searchWord) <= 0) {
            return Optional.empty();
        }

        //Gets the answer that the bot constructs
        String botAnswer = getBotAnswer(searchWord);
        return Optional.of(botAnswer);
    }

    @Override
    public String getBotName() {
        return "simplebot";
    }

    private String getBotAnswer(String searchWord) {
        // Gets a list of answers from the index matching the search word
        List<String> answers = index.search(searchWord);
        // Choose the first answer from the list of answers from the index
        String firstAnswer = answers.get(0);
        return searchWord+ " " + firstAnswer;
    }
}
