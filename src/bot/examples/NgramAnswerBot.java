package bot.examples;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import bot.AbstractSearchingAnswerBot;
import bot.BotAnswerPrinter;
import model.SearchIndex;

public class NgramAnswerBot extends AbstractSearchingAnswerBot {

	private final Random random = new Random();

	public NgramAnswerBot(SearchIndex index) {
		super(index);
	}

	/**
	 * This bot will take a random word from your question and build an answer
	 * using the random word as the key.
	 */
	@Override
	public Optional<String> answer(String question) {

		//Extracts a search word from the question
        ExampleQuestionParser questionParser = new ExampleQuestionParser();

		//TODO: Create your own question parser for extracting one or many search words
		String searchWord = questionParser.getWordToSearchOn(question);

		// Prints information it will search on
		BotAnswerPrinter.printBotWillHelpMessage(searchWord);

        // Checks if the chosen word can be found in the index
        // If the index doesn't contain the word an empty Optional is returned
        if (index.size(searchWord) <= 0) {
			return Optional.empty();
        }
		
		//Gets the answer that the bot constructs
		String botAnswer = getBotAnswer(searchWord);
		return Optional.of(botAnswer);
	}

    @Override
    public String getBotName() {
        return "ngrambot";
    }

    private String getBotAnswer(String word) {
		// The string builder that builds the answer. It will always
		// start with the chosen word from the question
		StringBuilder builder = new StringBuilder(word);
		
		// This loop appends five random answer ngrams from the index
		// and appends it 
		for (int i = 0; i < 6; i++) {

            // Gets the list of answer ngrams from the index
			List<String> answerNgrams = index.search(word);	

            if(answerNgrams.size() == 0){
                //Return result because it does not exists any more matching n-gram
                break;
            }
            // Chooses a random answer from the answer from the index
			String randomAnswer = answerNgrams.get(random.nextInt(answerNgrams.size()));

            // In the next loop run a new word will be searched on.
            // TODO: this can be changed, what happens if you search for the same word?
            String[] answerWords = randomAnswer.split(" ");
            word = answerWords[answerWords.length - 1];
			builder.append(" ");

            // Appends the answer string to the answer String builder
			builder.append(randomAnswer);
		}
		
		String answer = builder.toString();

        if(answer.contains(".")){
            // Cuts off everything after the last full stop to ensure
            // the answer ends with a full stop.
            answer = answer.substring(0, answer.lastIndexOf(".") + 1);
        }
		return answer;
	}
}
