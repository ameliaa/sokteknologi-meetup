package bot.examples;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ExampleQuestionParser {

    // TODO: Add or remove stopwords
    List<String> stopWords = Arrays.asList("och", "det", "att", "i", "en",
            "jag", "hon", "som", "han", "på", "den", "med", "var", "sig",
            "för", "så", "till", "är", "men", "ett", "om", "hade", "de", "av",
            "icke", "mig", "du", "henne", "då", "sin", "nu", "har", "inte",
            "hans", "honom", "skulle", "hennes", "där", "min", "man", "ej",
            "vid", "kunde", "något", "från", "ut", "när", "efter", "upp", "vi",
            "dem", "vara", "vad", "över", "än", "dig", "kan", "sina", "här",
            "ha", "mot", "alla", "under", "någon", "eller", "allt", "mycket",
            "sedan", "ju", "denna", "själv", "detta", "åt", "utan", "varit",
            "hur", "ingen", "mitt", "ni", "bli", "blev", "oss", "din", "dessa",
            "några", "deras", "blir", "mina", "samma", "vilken", "er", "sådan",
            "vår", "blivit", "dess", "inom", "mellan", "sådant", "varför",
            "varje", "vilka", "ditt", "vem", "vilket", "sitta", "sådana",
            "vart", "dina", "vars", "vårt", "våra", "ert", "era", "vilkas");

    private final Random random = new Random();

    public String getWordToSearchOn(String question) {

        //TODO Implement another clean method
        String cleanedQuestion = cleanQuestion(question);

        //TODO change how the search word is chosen
        // Splits the string into a list of words
        List<String> words = Arrays.asList(cleanedQuestion.split("\\s"));
        // Chooses the last word in the question as the word to search on.
        String searchWord = words.get(words.size()-1);

        //Another example on how to chose word is is to chose a random word
        // test this by uncommenting the row below:
        //searchWord = words.get(random.nextInt(words.size()));
        return searchWord;
    }

    private String cleanQuestion(String question) {
        question = question.toLowerCase();
        // Replaces all characters that are not letters,
        // digits or whitespace
        question = question.replaceAll("[^a-zåäöA-ZÅÄÖ0-9 ]", "");
        // Removes stop words listed above
        question = stripStopWords(question);
        //Removes extra whitespace
        question = question.replaceAll(" +", " ").trim();
        return question;
    }

    private String stripStopWords(String question) {
        question = " " + question + " ";
        // Removes all stop words from the question
        for (String word : stopWords) {
            question = question.replaceAll(" " + word + " ", " ");
        }
        return question;
    }

}
